const VError = require( 'verror' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    return ( message, eventContext ) => {
      // Local libraries
      const libs = context.libs

      // Initialized node packages
      // `firebase-admin`
      const admin = context.admin
      // `firebase`
      const firebase = context.firebase
      // `@google-cloud/pubsub`
      const pubsub = context.pubsub
      // `@google-cloud/storage`
      const storage = context.storage

      /*
         * Start
         * module.exports.start
         */

      let _start = {}

      /** @function start
         * Start of event handling.
         */
      let start = ( params = {} ) => {
        let _methodName = 'start'
        let path = eventDatabasePath()

        return libs.database.transaction( {
          path: path,
          func: node => {
            if ( node === null ) {
              return {
                status: 'ready',
                timestamp: eventContext.timestamp
              }
            } else if ( node.status === 'error' ) {
              node.status = 'ready'
              return node
            }
          }

        }, ( error, committed, dataSnapshot ) => {
          if ( error ) {
            let transactionError = libs.error.create( {
              error: error,
              message: 'Could not check Pub/Sub event status',
              name: _methodName
            } )

            console.error( transactionError )
          } else if ( committed === false ) {
            let commitError = new VError( { name: _methodName }, 'Could not commit to database for Pub/Sub event' )
            let transactionError = libs.error.create( {
              error: commitError,
              message: 'Could not check Pub/Sub event status',
              name: _methodName
            } )

            console.error( transactionError )
          }
        } )

          .then( result => {
            let value = result.snapshot.val()

            // Only proceed with event if it is ready or if it has errored
            if ( value.status === 'ready' || value.status === 'error' ) {
              // Update status to in progress
              return libs.database.set( { path: `${path}/status`, value: 'in-progress' } )

                .then( data => {
                  // Should proceed on error
                  if ( data instanceof Error ) { console.error( new VError( data, 'Could not set status to "in-progress" for event' ) ) }

                  return true
                } )
            }

            console.log( 'Event has non-ready status:', value.status )

            // Consider returning informative response
            return false
          } )
      }

      _start = start

      /*
         * End
         * module.exports.end
         */

      let _end = {}

      /** @function end
         * End of event handling.
         * @param {*} result - Represents the return value of the triggered function.
         * @returns {null}
         */
      let end = ( result = {} ) => {
        let _methodName = 'end'

        return endStatusUpdate( { result: result, settings: settings } )

          .then( data => {
            if ( data instanceof Error ) {
              console.error( libs.error.create( {
                error: data,
                message: 'Could not set event status in database',
                name: _methodName
              } ) )
            }

            // Pub/Sub was originated by this project
            // Current project id matches requester project id from attributes
            if ( libs.project.id() === endRequesterId() ) {
              return endGatewayInput( { result: result, settings: settings } )

              // Pub/Sub was originated by another project
            } else {
              return endGatewayOutput( { result: result, settings: settings } )
            }
          } )

          .then( data => {
            if ( data instanceof Error ) {
              console.error( libs.error.create( {
                error: data,
                message: 'Could not send result to gateway.',
                name: _methodName
              } ) )
            } else {
              console.log( 'Successfully sent result.' )
            }
          } )
      }

      _end = end

      /*
         * End Status
         * module.exports.end.status
         */

      _end.status = {}

      /** @function end.status
         * The status of the event after it has been handled.
         * @param {*} result - Represents the return value of the triggered function.
         * @returns {string} - The status message for the result.
         */
      let endStatus = ( result = {} ) => {
        // let _methodName = 'end-status'

        // Final return value of the function was an error
        if ( result instanceof Error ) { return 'error' }

        // Non-errors are considered a success
        return 'success'
      }

      _end.status = endStatus

      /** @function end.status.update
         * Changes the status for the event in the Realtime Database.
         * @param {*} result - Represents the return value of the triggered function.
         * @returns {null|Error} - Null if successful, or an error.
         */
      let endStatusUpdate = ( result = {} ) => {
        // let _methodName = 'end-status-update'
        // Location of event data in realtime database
        let eventPath = eventDatabasePath()
        let _status = endStatus( { result: result } )

        // Set status in realtime database
        return libs.database.set( {
          path: `${eventPath}/status`,
          value: _status
        } )
      }

      _end.status.update = endStatusUpdate

      /*
         * End Gateway
         * module.exports.end.status
         */

      _end.gateway = {}

      /** @function end.gateway.message
         * Generates the message object to be sent to the gateway topic.
         * @param {*} result - Represents the return value of the triggered function.
         * @returns {Object} - Formatted message object with the event, JSON payload, and result of the operation.
         */
      let endGatewayMessage = ( result = {} ) => {
        // let _methodName = 'end-gateway-message'

        return {
          // event: event,
          event: eventContext,
          payload: message.json,
          result: result
        }
      }

      _end.gateway.message = endGatewayMessage

      /** @function end.gateway.input
         * Send result of operation to this project's gateway input.
         * @param {*} result - Represents the return value of the triggered function.
         * @returns {string|Error} - Message Id from publishing to the topic or an error.
         */
      let endGatewayInput = ( result = {} ) => {
        // let _methodName = 'end-gateway-input'
        let topic = 'gateway_input'
        let message = endGatewayMessage( { result: result } )
        let attributes = message.attributes || {}

        // Status is used at `gateway_input` to determine the result of the Pub/Sub function
        let _status = endStatus( { result: result } )

        console.log( `Sending result to "${topic}".` )

        // Publishes to `gateway_input`
        // Pub/Sub result handling is managed there
        return libs.pubsub.publish( {
          topic: topic,
          message: message,
          attributes: {
            _id: attributes._id,
            _status: _status
          }
        } )
      }

      _end.gateway.input = endGatewayInput

      /** @function end.gateway.output
         * Send result of operation to this project's gateway output, to be redirected to destination project.
         * @param {*} result - Represents the return value of the triggered function.
         * @returns {string|Error} - Message Id from publishing to the topic or an error.
         */
      let endGatewayOutput = ( result = {} ) => {
        // let _methodName = 'end-gateway-output'
        // `gateway_output` handles cross-project Pub/Sub messages
        let topic = 'gateway_output'
        let destinationTopic = 'gateway_input'
        let message = endGatewayMessage( { result: result } )
        let attributes = message.attributes || {}

        // Status is used at `gateway_input` to determine the result of the Pub/Sub function
        let _status = endStatus( { result: result } )

        console.log( `Sending result to "${topic}".` )
        console.log( `Destination is "${destinationTopic}" at project "${endRequesterId()}"` )

        // Publishes to `gateway_output`
        // Message is delivered to `gateway_input` at the originating project
        return libs.pubsub.publish( {
          topic: topic,
          message: message,
          attributes: {
            _id: attributes._id,
            _status: _status,
            topic: destinationTopic,
            projectId: endRequesterId()
          }
        } )
      }

      _end.gateway.output = endGatewayOutput

      /*
         * End Requester
         * module.exports.end.requester
         */

      _end.requester = {}

      /** @function end.request.id
         * Gets the project id for the requesting project that triggered this event.
         * @returns {string} - The project id for the requesting project. Defaults to current project.
         */
      let endRequesterId = () => {
        // let _methodName = 'end-requester-id'
        // The originator project of this Pub/Sub trigger call
        // Defaults to current project
        return message.attributes._requester || libs.project.id()
      }

      _end.requester.id = endRequesterId

      /*
         * Event
         * module.exports.event
         */

      let _event = {}

      // Sample execution would be helpful
      /** @function event.validate
         * Ensures `event.data.json` is
         * available and performs any additional checks
         * provided by caller.
         * @param {Object} params - (Optional) The parameter object.
         * @param {Array} params.checklist - (Optional) Checks to be performed for the event. These checks are
         * functions which make some evaluation and return
         * an error if the result is unsatisfactory.
         * Ideally, these errors should have an appropriate
         * and informative `name` and `message`.
         * @returns {Error|null} An error if validation fails, otherwise null.
         */
      let eventValidate = ( params = {} ) => {
        let _methodName = 'event-validate'

        return new Promise( ( resolve, reject ) => {
          // Perform fundemental Pub/Sub checks
          if ( message.json ) {
            let error = new VError( { name: _methodName }, 'Missing "json" in message data' )

            resolve( libs.error.create( {
              error: error,
              message: 'Could not validate event.',
              name: _methodName
            } ) )
          }

          let errors = []

          // Evaluate provided checklist
          let checklist = params.checklist

          for ( let check of checklist ) {
            // Perform a check
            let result = check()

            // If result of check is an error then add the error(s) to the list
            if ( result instanceof Error ) { VError.errorForEach( result, error => { errors.push( error ) } ) }
          }

          // There are errors.
          if ( errors.length ) {
            resolve(
              libs.error.create( {
                error: new VError.MultiError( errors ),
                message: 'Event did not pass required checks.',
                name: _methodName
              } )
            )
          }

          // No errors. Passed all checks.
          resolve()
        } )
      }

      _event.validate = eventValidate

      /*
         * Event Database
         * module.exports.event.database
         * Events are tracked in database primarily for idempotency.
         */

      _event.database = {}

      /** @function event.database.path
         * Provides the path for the event in the Realtime Database.
         * @returns {string} Path for event in database.
         */
      let eventDatabasePath = () => {
        // let _methodName = 'event-database-path'

        return `@coldandgoji/pubsub/event/${eventContext.eventId}`
      }

      _event.database.path = eventDatabasePath

      /*
         * Project
         * module.exports.project
         */

      let _project = {}

      /** @function project.initialize
         * Instantiates this Pub/Sub Trigger library for a given project.
         * This involves passing service account credentials along with the project id to the necessary dependencies.
         * @param {Object} params - The parameter object.
         * @param {string} params.projectId - The id for the project you wish to act on behalf of.
         * @param {Object} params.serviceAccount - (Optional) JSON formatted service account data. Will default to a service account passed in when loading the main library.
         * @returns {Object} An instance of this library initialized for a specific project.
         */
      let initializeProject = ( params = {} ) => {
        // let _methodName = 'project-initialize'
        let projectContext = libs.project.context.initialize( {
          context: context,
          projectId: params.projectId,
          serviceAccount: params.serviceAccount || context.serviceAccount
        } )

        return require( './index' )( projectContext )( message, eventContext )
      }
      _project.initialize = initializeProject

      /*
         * User
         * module.exports.user
         */

      let _user = {}

      /*
       * User Claims
       * module.exports.user.claims
       */

      _user.claims = {}

      let setUserClaims = ( params = {} ) => {
        return admin.auth().setCustomUserClaims( params.uid, params.claims ).catch( libs.error.admin.auth )
      }
      _user.claims.set = setUserClaims

      /*
         * User Creation
         * module.user.create
         */

      /** @function user.create
         * Create a user in Firebase Auth and a user node in the database..
         * @param {Object} params - The parameter object.
         * @param {Object} params.auth - Properties to be saved for the auth user.
         * @param {Object} params.database - Properties to be indexed for the database user.
         * @returns {Promise} An error or null if successful.
         */
      const userCreate = ( params = {} ) => {
        let _methodName = 'user-create'

        return userCreateAuth( { properties: params.auth } )

          .then( userRecord => {
            if ( userRecord instanceof Error ) {
              return libs.error.create( {
                error: userRecord,
                message: 'Could not create user node in database.',
                name: _methodName
              } )
            } else {
              return userCreateDatabase( {
                properties: params.database,
                uid: userRecord.uid
              } )
            }
          } )

          .then( data => {
            if ( data instanceof Error ) {
              return libs.error.create( {
                error: data,
                message: 'Could not create user.',
                name: _methodName
              } )
            } else {
              return data
            }
          } )
      }

      _user.create = userCreate

      /** @function user.create.auth
         * Create a user in Firebase Auth.
         * @param {Object} params - The parameter object.
         * @param {Object} params.properties - Properties to be saved for the user.
         * @returns {Promise} A Firebase Auth user record or an error.
         */
      const userCreateAuth = ( params = {} ) => {
        let _methodName = 'user-create'

        return admin.auth().createUser( params.properties )

          .catch( error => {
            return libs.error.create( {
              error: libs.error.admin.auth( error ),
              message: 'Could not create auth user.',
              name: _methodName
            } )
          } )
      }

      _user.create = userCreateAuth

      /** @function user.create.database
         * Create a user node in the database.
         * @param {Object} params - The parameter object.
         * @param {string} params.uid - The user id.
         * @param {Object} params.properties - Properties to be indexed for the user.
         * @returns {Promise} An error if cannot set database node or if user node exists, or null if successful.
         */
      const userCreateDatabase = ( params = {} ) => {
        let _methodName = 'user-create-database'
        let userPath = libs.user.database.path( params.uid )
        let properties = params.properties || {}

        properties.id = params.uid

        return libs.database.fetch( { path: userPath } )

          .then( dataSnapshot => {
            if ( dataSnapshot instanceof Error ) {
              return libs.error.create( {
                error: dataSnapshot,
                message: 'Could not fetch user node from database.',
                name: _methodName
              } )
            } else if ( !dataSnapshot.val() ) {
              return libs.database.set( {
                path: userPath,
                value: properties
              } )
            } else {
              let error = new VError( { name: 'database/user-exists' }, 'Cannot overwrite existing user in database.' )
              return libs.error.create( {
                error: error,
                info: {
                  code: 403,
                  response: {
                    message: error.message,
                    status: error.name
                  }
                },
                message: 'Could not create user node in database.',
                name: _methodName
              } )
            }
          } )
      }

      _user.create.database = userCreateDatabase

      /*
         * User References
         * module.exports.user.reference
         * Handles references for a user.
         * References are represented as IDs in user properties which reference other database objects.
         * References are indexed by GUIDs resulting from a `push` to the database.
         * i.e. `user/{uid}/invoice/{guid}` = `{id}`, referencing `invoice/{id}`
         */

      _user.reference = {}

      /** @function user.reference.fetch
         * Fetch a reference for a user.
         * @param {Object} params - The parameter object.
         * @param {string} params.key - The key the property is indexed at.
         * @param {string} params.uid - The user id.
         * @returns {Promise} The value of the referenced node or an error.
         */
      const userReferenceFetch = ( params = {} ) => {
        let _methodName = 'user-reference-fetch'

        return userPropertyFetch( params )

          .then( dataSnapshot => {
            if ( dataSnapshot instanceof Error ) {
              return libs.error.create( {
                error: dataSnapshot,
                message: 'Could not resolve user reference.',
                name: _methodName
              } )
            }

            return libs.database.reference.resolve( dataSnapshot )
          } )
      }

      _user.reference.fetch = userReferenceFetch

      /*
         * User Properties
         * module.exports.user.property
         * Properties are the represented as child nodes for a given user.
         * i.e. `user/{uid}/{property}
         */

      _user.property = {}

      /** @function user.property.fetch
         * Fetch a property for a user.
         * @param {Object} params - The parameter object.
         * @param {string} params.key - The key the property is indexed at.
         * @param {string} params.uid - The user id.
         * @returns {Promise} A `dataSnapshot` of a user node or an error.
         */
      const userPropertyFetch = ( params = {} ) => {
        let _methodName = 'user-property-fetch'
        let userPath = libs.user.database.path( params.uid )

        return libs.database.fetch( {
          path: `${userPath}/${params.key}`
        } )

          .then( dataSnapshot => {
            if ( dataSnapshot instanceof Error ) {
              return libs.error.create( {
                error: dataSnapshot,
                message: 'Could not fetch user property.',
                name: _methodName
              } )
            }

            return dataSnapshot
          } )
      }

      _user.property.fetch = userPropertyFetch

      /** @function user.property.set
         * Set a property for a user.
         * @param {Object} params - The parameter object.
         * @param {string} params.key - The key the property is indexed at.
         * @param {*} params.value - The value of the property to be indexed.
         * @param {string} params.uid - The user id.
         * @returns {Promise} An error or null if successful.
         */
      const userPropertySet = ( params = {} ) => {
        let _methodName = 'user-property-set'
        let userPath = libs.user.database.path( params.uid )

        return libs.database.set( {
          path: `${userPath}/${params.key}`,
          value: params.value
        } )

          .then( data => {
            if ( data instanceof Error ) {
              return libs.error.create( {
                error: data,
                message: 'Could not set user property.',
                name: _methodName
              } )
            }

            return data
          } )
      }

      _user.property.set = userPropertySet

      /** @function user.property.push
         * Push to a property for a user.
         * @param {Object} params - The parameter object.
         * @param {string} params.key - The key the property is indexed at.
         * @param {*} params.value - The value of the property to be indexed.
         * @param {string} params.uid - The user id.
         * @returns {Promise} A reference to the newly created node or an error.
         */
      const userPropertyPush = ( params = {} ) => {
        let _methodName = 'user-property-push'
        let userPath = libs.user.database.path( params.uid )

        return libs.database.push( {
          path: `${userPath}/${params.key}`,
          value: params.value
        } )

          .then( reference => {
            if ( reference instanceof Error ) {
              return libs.error.create( {
                error: reference,
                message: 'Could not push user property.',
                name: _methodName
              } )
            }

            return reference
          } )
      }

      _user.property.push = userPropertyPush

      /*
         * User Retrieval
         * module.exports.user.fetch
         * Currently refers only to getting the user as represented in the database. Should be fleshed out
         * to handle auth requests as well.
         */

      // This should probably be refactored to not error on partial success
      /** @function user.fetch
         * Fetch a given or authorized user.
         * @param {Object} params - The parameter object.
         * @param {string} params.uid - The user id. (Optional, if user can be authorized)
         * @returns {Promise} The Firebase Auth user record and the user's corresponding database node, or an error.
         */
      let userFetch = ( params = {} ) => {
        let _methodName = 'user-fetch'

        return Promise.all( [
          userFetchAuth( params, settings ),
          userFetchDatabase( params, settings )
        ] )

          .then( data => {
            if ( data instanceof Error ) {
              return libs.error.create( {
                error: data,
                info: {
                  https: {
                    code: 500,
                    response: {
                      message: 'Could not fetch user.'
                    }
                  }
                },
                message: 'Could not fetch user.',
                name: _methodName
              } )
            }

            for ( let item of data ) {
              if ( item instanceof Error ) {
                return libs.error.create( {
                  error: item,
                  info: {
                    https: {
                      code: 500,
                      response: {
                        message: 'Could not fetch user.'
                      }
                    }
                  },
                  message: 'Could not fetch user.',
                  name: _methodName
                } )
              }
            }

            return {
              userRecord: data[ 0 ],
              userData: data[ 1 ].val()
            }
          } )
      }

      _user.fetch = userFetch

      /** @function user.fetch.auth
         * Fetch the user record for a given or authorized user.
         * @param {Object} params - The parameter object.
         * @param {string} params.uid - The user id.
         * @returns {Promise} A Firebase Auth user record or an error.
         */
      let userFetchAuth = ( params = {} ) => {
        let _methodName = 'user-fetch-auth'

        return admin.auth().getUser( params.uid )

          .catch( error => {
            return libs.error.create( {
              error: libs.error.admin.auth( error ),
              message: 'Could not fetch user.',
              name: _methodName
            } )
          } )
      }

      _user.fetch.auth = userFetchAuth

      /** @function user.fetch.database
         * Fetch the database node for a given or authorized user.
         * @param {Object} params - The parameter object.
         * @param {string} params.uid - The user id.
         * @returns {Promise} A `dataSnapshot` for a user's Firebase Realtime Database node, or an error.
         */
      let userFetchDatabase = ( params = {} ) => {
        let _methodName = 'user-fetch-database'
        let userPath = libs.user.database.path( params.uid )

        return libs.database.fetch( { path: userPath } )

          .then( dataSnapshot => {
            if ( dataSnapshot instanceof Error ) {
              return libs.error.create( {
                error: dataSnapshot,
                message: 'Could not fetch user from database.',
                name: _methodName
              } )
            }

            return dataSnapshot
          } )
      }

      _user.fetch.database = userFetchDatabase

      return {
        // Trigger libraries
        end: _end,
        event: _event,
        project: _project,
        start: _start,
        user: _user,
        // Local Libraries
        libs: libs,
        // Initialized packages
        package: {
          admin: admin,
          firebase: firebase,
          pubsub: pubsub,
          storage: storage
        }
      }
    }
  }
}
