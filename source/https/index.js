const _ = require( 'lodash' )
const VError = require( 'verror' )
const stringify = require( 'json-stringify-safe' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    return ( request, response ) => {
      // Local libraries
      let libs = context.libs

      // Initialized node packages
      // `firebase-admin`
      let admin = context.admin
      // `firebase`
      let firebase = context.firebase
      // `@google-cloud/pubsub`
      let pubsub = context.pubsub
      // `@google-cloud/storage`
      let storage = context.storage

      /*
       * Response
       * module.exports.response
       * Manages the express `response` object
       */

      let _response = {}

      /*
       * Response Headers
       * module.exports.response.header
       * Manage header properties in the response.
       */

      /** @function response.header
       * Change headers of the response. Designed to replicate standard express functionality.
       * This addesses the issue of headers being set by the caller not showing up in the response
       * when using: coldcloud.end()
       * @param {string} key - The key of the header to be set.
       * @param {string} value - The value of the header to be set.
       * @returns {null}
       */
      let responseHeader = ( key = '', value = '' ) => {
        // let _methodName = 'response-header'
        response.header( key, value )
      }

      _response.header = responseHeader

      /*
       * Response Locals
       * module.exports.response.locals
       * @response-locals
       */
      _response.locals = {}

      /** @function response.locals.set
       * Set value to locals object in response.
       * @param {string} key - The key of the header to be set.
       * @param {string} value - The value of the header to be set.
       * @returns {null}
       */
      let setResponseLocals = ( params = {} ) => {
        // let _methodName = 'response-locals-set'
        response.locals[ params.key ] = params.value
      }

      _response.locals.set = setResponseLocals

      /*
       * Project
       * module.exports.project
       * Builds on local project libs.
       */

      let _project = {}

      /** @function project.initialize
       * Instantiates this HTTPS Trigger library for a given project.
       * This involves passing service account credentials along with the project id to the necessary dependencies.
       * @param {Object} params - The parameter object.
       * @param {string} params.projectId - The id for the project you wish to act on behalf of.
       * @param {Object} params.serviceAccount - (Optional) JSON formatted service account data. Will default to a service account passed in when loading the main library.
       * @returns {Object} An instance of this library initialized for a specific project.
       */
      let initializeProject = ( params = {} ) => {
        // let _methodName = 'project-initialize'
        let projectContext = libs.project.context.initialize( {
          context: context,
          projectId: params.projectId,
          serviceAccount: params.serviceAccount || context.serviceAccount
        } )

        return require( './index' )( projectContext )( event )
      }
      _project.initialize = initializeProject

      /*
       * End
       * module.exports.end
       */

      let _end = {}

      /** @function end
       * End of event response handling for HTTPS.
       * Paramters are optional. Response from most recent response producing function execution is used by default. Most, if not all, public functions change the cached response on success.
       * @param {Object} params - (Optional) The parameter object.
       * @param {string} params.code - (Optional) Three digit status code. i.e. 200, 400
       * @param {Object} params.response - (Optional) The JSON response to be returned to the caller.
       * @param {*} params.response.body - (Optional) Any additional data to be returned to the caller.
       * @param {string} params.response.message - (Optional) Explanation of the outcome.
       * @param {string} params.response.status - (Optional) Parameterized short message with essential details.
       * @returns {null}
       */
      let end = ( result = {} ) => {
        // let _methodName = 'end'

        return new Promise( ( resolve, reject ) => {
          if ( result instanceof Error ) {
            libs.log( 'Ending with error' )

            // `https.create` renders an object with:
            //   - `code`: (Number) A HTTP status code
            //   - `response`: (Object)
            //   - `response.status`: (String) A parameterized string that gives more detail about the error.
            //   - `response.message`: (String) A longer explaination of the error.
            let data = libs.error.https.create( result )

            try {
              // This is going to be too much
              // Just need to figure out what combination of data
              // is most useful

              // Error message
              libs.log( 'Message:', _.join( _.split( result.message, ':' ), '\n' ) )

              // Error info
              libs.log( 'Info:', stringify( VError.info( result ) ) )

              // Error cause
              let rootCause = VError.cause( result ) || result
              while ( VError.cause( rootCause ) ) { rootCause = VError.cause( rootCause ) }
              libs.log( 'Cause:', rootCause )
            } catch ( err ) {
              libs.log( 'Problem logging error details:', err )
            }

            libs.log( `(${data.code})`, 'Ending with failure:', stringify( data.response ) )

            resolve( response.status( data.code ).json( data.response ) )
          } else {
            let jsonResponse = result.response || {}
            let statusCode = result.code || 200

            libs.log( `(${statusCode})`, 'Ending with success:', stringify( jsonResponse ) )

            resolve( response.status( statusCode ).json( jsonResponse ) )
          }
        } )
      }

      _end = end

      /*
       * Authorize
       * module.exports.authorize
       * Checks 'Authorization' header: request.get( 'Authorization' );
       * Takes the "Bearer <token>" and processes it as a Firebase Auth token.
       * Consider indexing under user. ( i.e. user.authorize.bearer() )
       */

      let _authorize = {}

      /** @function authorize.bearer
       * Authorize a user for Firebase Auth.
       * @param {Object} params - The parameter object.
       * @param {string} token - (Optional) The token to be verified.
       * @returns {Promise} A `decodedIdToken` with Firebase Auth user data and an `idToken` or an error.
       */
      let authorizeBearer = ( params = {} ) => {
        let _methodName = 'authorize-bearer'
        let token = params.token || bearerToken()

        if ( token instanceof Error ) {
          return Promise.resolve(
            libs.error.create( {
              error: token,
              message: 'Could not verify bearer token',
              name: _methodName
            } )
          )
        }

        libs.log( 'Authorizing bearer...', token )

        return verifyToken( { token: token } )
          .then( data => {
            if ( data instanceof Error ) {
              return libs.error.create( {
                error: data,
                message: 'Could not authorize bearer.',
                name: _methodName
              } )
            }

            // data
            // - idToken
            // - decodedIdToken
            return data
          } )
      }

      _authorize.bearer = authorizeBearer

      /** @function authorize.bearerWithData
       * Authorize a user for Fireebase Auth and fetch their data.
       * @param {Object} params - The parameter object.
       * @param {string} token - (Optional) The token to be verified.
       * @returns {Promise} A `decodedIdToken` with Firebase Auth user data and the user's corresponding database node or an error.
       */
      let authorizeBearerWithData = ( params = {} ) => {
        let _methodName = 'authorize-bearer-with-data'
        let body = {}

        return authorizeBearer( params )

          .then( data => {
            if ( data instanceof Error ) {
              return libs.error.create( {
                error: data,
                message: 'Could not fetch user from database.',
                name: _methodName
              } )
            }

            // decodedIdToken
            // idToken
            body = data

            return userFetchDatabase( data.decodedIdToken )
          } )

          .then( dataSnapshot => {
            if ( dataSnapshot instanceof Error ) {
              return libs.error.create( {
                error: dataSnapshot,
                message: 'Could not authorize bearer with data.',
                name: _methodName
              } )
            }

            body.userData = dataSnapshot.val()

            return body
          } )
      }

      _authorize.bearerWithData = authorizeBearerWithData

      /*
       * User
       * module.exports.user
       */

      let _user = {}

      /*
       * User Claims
       * module.exports.user.claims
       */

      _user.claims = {}

      let setUserClaims = ( params = {} ) => {
        return admin.auth().setCustomUserClaims( params.uid, params.claims ).catch( libs.error.admin.auth )
      }
      _user.claims.set = setUserClaims

      /*
       * User Login
       * module.exports.user.login
       */

      /** @function userLogin
       * Render Firebase User object. This is not for user authentication.
       * @param {Object} params - The parameter object.
       * @param {string} params.uid - The user id.
       * @returns {Promise} An id token for a user or an error.
       */
      let userLogin = ( params = {} ) => {
        let _methodName = 'user-login'

        return admin.auth().createCustomToken( params.uid )

          .catch( error => { return libs.error.admin.auth( error ) } )

          .then( customToken => {
            if ( customToken instanceof Error ) {
              return libs.error.create( {
                error: customToken,
                message: 'Could not sign in with custom token',
                name: _methodName
              } )
            }

            return firebase.auth().signInWithCustomToken( customToken )

              .catch( error => { return libs.error.firebase.auth( error ) } )
          } )
      }

      _user.login = userLogin

      /** @function user.login.email
       * Login a registered Firebase Auth user.
       * @param {Object} params - (Optional) The parameter object.
       * @param {string} params.email - (Optional) Email address for registered user.
       * @param {string} params.password - (Optional) Password for the registered user.
       * @param {boolean} params.forceRefresh - (Optional) Forces id token to be refreshed, even if still valid.
       * @returns {Promise} A user id, `uid`, and an `idToken`, both strings, or an error.
       */
      let userLoginEmail = ( params = {} ) => {
        let _methodName = 'user-login-email'
        let email = params.email || request.body.email
        let password = params.password || request.body.password
        let user

        return firebase.auth().signInWithEmailAndPassword( email, password )

          .catch( error => { return libs.error.firebase.auth( error ) } )

          .then( firebaseUser => {
            if ( firebaseUser instanceof Error ) {
              let message = 'Could not get id token for user.'

              return libs.error.create( {
                error: firebaseUser,
                message: message,
                name: _methodName
              } )
            } else {
              user = firebaseUser

              return user.getIdToken( params.forceRefresh )
            }
          } )

          .then( idToken => {
            if ( idToken instanceof Error ) {
              return libs.error.create( {
                error: idToken,
                message: 'Could not login user with email and password.',
                name: _methodName
              } )
            } else {
              return {
                idToken: idToken,
                uid: user.uid
              }
            }
          } )
      }

      _user.login.email = userLoginEmail

      /** @function user.login.emailWithData
       * Login a registered Firebase Auth user.
       * @param {Object} params - (Optional) The parameter object.
       * @param {string} params.email - (Optional) Email address for registered user.
       * @param {string} params.password - (Optional) Password for the registered user.
       * @param {boolean} params.forceRefresh - (Optional) Forces id token to be refreshed, even if still valid.
       * @returns {Promise} A user id, `uid`, an `idToken` and the contents of the corresponding user database node, or an error.
       */
      let userLoginEmailWithData = ( params = {} ) => {
        let _methodName = 'user-login-email-with-data'
        let body

        return userLoginEmail( params )

          .then( data => {
            if ( data instanceof Error ) {
              return libs.error.create( {
                error: data,
                message: 'Could not fetch user from database.',
                name: _methodName
              } )
            } else {
              body = data

              return userFetchDatabase( data )
            }
          } )

          .then( dataSnapshot => {
            if ( dataSnapshot instanceof Error ) {
              return libs.error.create( {
                error: dataSnapshot,
                message: 'Could not login user with email and password with data.',
                name: _methodName
              } )
            } else {
              body.userData = dataSnapshot.val()

              return body
            }
          } )
      }

      _user.login.emailWithData = userLoginEmailWithData

      /*
       * User Retrieval
       * module.exports.user.fetch
       * Currently refers only to getting the user as represented in the database. Should be fleshed out
       * to handle auth requests as well.
       */

      /** @function user.fetch
       * Fetch a given or authorized user.
       * @param {Object} params - The parameter object.
       * @param {string} params.uid - The user id. (Optional, if user can be authorized)
       * @returns {Promise} The record and database node for the user, or an error.
       */
      let userFetch = ( params = {} ) => {
        let _methodName = 'user-fetch'

        if ( params.uid ) {
          return Promise.all( [
            userFetchAuth( params ),
            userFetchDatabase( params )
          ] )

            .catch( error => { return error } )

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  info: {
                    https: {
                      code: 500,
                      response: {
                        message: 'Could not fetch user.'
                      }
                    }
                  },
                  message: 'Could not fetch user.',
                  name: _methodName
                } )
              }

              for ( let item of data ) {
                if ( item instanceof Error ) {
                  return libs.error.create( {
                    error: item,
                    info: {
                      https: {
                        code: 500,
                        response: {
                          message: 'Could not fetch user.'
                        }
                      }
                    },
                    message: 'Could not fetch user.',
                    name: _methodName
                  } )
                }
              }

              return {
                userRecord: data[ 0 ],
                userData: data[ 1 ].val()
              }
            } )
        } else {
          return authorizeBearer()

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  message: 'Could not fetch user.',
                  name: _methodName
                } )
              } else {
                params.uid = data.decodedIdToken.uid

                return userFetch( params )
              }
            } )
        }
      }

      _user.fetch = userFetch

      /** @function user.fetch.auth
       * Fetch the user record for a given or authorized user.
       * @param {Object} params - The parameter object.
       * @param {string} params.uid - The user id. (Optional, if user can be authorized)
       * @returns {Promise} The Firebase Auth user record or an error.
       */
      let userFetchAuth = ( params = {} ) => {
        let _methodName = 'user-fetch-auth'

        if ( params.uid ) {
          return admin.auth().getUser( params.uid )

            .catch( error => { return libs.error.admin.auth( error ) } )

            .then( userRecord => {
              if ( userRecord instanceof Error ) {
                return libs.error.create( {
                  error: userRecord,
                  message: 'Could not fetch user.',
                  name: _methodName
                } )
              }

              return userRecord
            } )
        } else {
          return authorizeBearer()

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  message: 'Could not fetch user.',
                  name: _methodName
                } )
              } else {
                params.uid = data.decodedIdToken.uid

                return userFetchAuth( params )
              }
            } )
        }
      }

      _user.fetch.auth = userFetchAuth

      /** @function user.fetch.database
       * Fetch the database node for a given or authorized user.
       * @param {Object} params - The parameter object.
       * @param {string} params.uid - The user id. (Optional, if user can be authorized)
       * @returns {Promise} The user's node in the Firebase Realtime Database or an error.
       */
      let userFetchDatabase = ( params = {} ) => {
        let _methodName = 'user-fetch-database'

        if ( params.uid ) {
          let userPath = libs.user.database.path( params.uid )

          return libs.database.fetch( { path: userPath } )

            .then( dataSnapshot => {
              if ( dataSnapshot instanceof Error ) {
                return libs.error.create( {
                  error: dataSnapshot,
                  message: 'Could not fetch user from database.',
                  name: _methodName
                } )
              }

              return dataSnapshot
            } )
        } else {
          return authorizeBearer()

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  message: 'Could not fetch user from database.',
                  name: _methodName
                } )
              } else {
                params.uid = data.decodedIdToken.uid

                return userFetchDatabase( params )
              }
            } )
        }
      }

      _user.fetch.database = userFetchDatabase

      /*
       * User Creation
       * module.user.create
       */

      /** @function user.create
       * Create a user in Firebase Auth and a user node in Firebase Database.
       * @param {Object} params - The parameter object.
       * @param {Object} params.auth - Properties to be saved for the auth user.
       * @param {Object} params.database - Properties to be indexed for the database user.
       * @returns {Promise} The record and database node for the user, or an error.
       */
      let userCreate = ( params = {} ) => {
        let _methodName = 'user-create'
        let record

        return userCreateAuth( { properties: params.auth } )

          .then( userRecord => {
            if ( userRecord instanceof Error ) {
              return libs.error.create( {
                error: userRecord,
                message: 'Could not create user node in database.',
                name: _methodName
              } )
            } else {
              record = userRecord

              return userCreateDatabase( {
                properties: params.database,
                uid: userRecord.uid
              } )
            }
          } )

          .then( userData => {
            if ( userData instanceof Error ) {
              return libs.error.create( {
                error: userData,
                message: 'Could not create user.',
                name: _methodName
              } )
            } else {
              return {
                userData: userData,
                userRecord: record
              }
            }
          } )
      }

      _user.create = userCreate

      /** @function userCreateAuth
       * Create a user in Firebase Auth.
       * @param {Object} params - The parameter object.
       * @param {Object} params.properties - Properties to be saved for the user.
       * @param {Object} params.properties.claims - (Optional) Will be set as claims for the user.
       * @returns {Promise} The Firebase Auth user record or an error.
       */
      let userCreateAuth = ( params = {} ) => {
        let _methodName = 'user-create-auth'
        let properties = params.properties

        libs.log( 'Creating Firebase Auth User...', properties )

        return admin.auth().createUser( properties )

          .catch( error => { return libs.error.admin.auth( error ) } )

          .then( userRecord => {
            if ( userRecord instanceof Error ) {
              return libs.error.create( {
                error: userRecord,
                message: 'Could not create auth user.',
                name: _methodName
              } )
            }

            if ( properties.claims ) {
              return admin.auth().setCustomUserClaims( userRecord.uid, properties.claims )

                .catch( error => { return libs.error.admin.auth( error ) } )

                .then( data => {
                  if ( data instanceof Error ) {
                    let error = libs.error.create( {
                      error: data,
                      message: 'Could not set custom user claims',
                      name: _methodName
                    } )

                    // Still succeed when custom claims are not set
                    console.error( error )
                  }

                  return userRecord
                } )
            } else {
              return userRecord
            }
          } )
      }

      _user.create.auth = userCreateAuth

      /** @function userCreateDatabase
       * Create a user node in the database.
       * @param {Object} params - The parameter object.
       * @param {string} params.uid - The user id.
       * @param {Object} params.properties - Properties to be indexed for the user.
       * @param {Object} params.properties.claims - (Optional) Will be set as claims for the auth user.
       * @returns {Promise} The user's node in the Firebase Realtime Database or an error.
       */
      let userCreateDatabase = ( params = {} ) => {
        let _methodName = 'user-create-database'
        let userPath = libs.user.database.path( params.uid )
        let properties = params.properties || {}

        properties.id = properties.id || params.uid

        libs.log( 'Creating Firebase User in Database', params )

        return libs.database.fetch( { path: userPath } )

          .then( dataSnapshot => {
            if ( dataSnapshot instanceof Error ) {
              return libs.error.create( {
                error: dataSnapshot,
                message: 'Could not fetch user node from database.',
                name: _methodName
              } )
            } else if ( !dataSnapshot.val() ) {
              return libs.database.set( {
                path: userPath,
                value: properties
              } )
            } else {
              let error = new VError( { name: 'database/user-exists' }, 'Cannot overwrite existing user in database.' )
              return libs.error.create( {
                error: error,
                info: {
                  code: 403,
                  response: {
                    message: error.message,
                    status: error.name
                  }
                },
                message: 'Could not create user node in database.',
                name: _methodName
              } )
            }
          } )

          .then( data => {
            if ( data instanceof Error ) {
              return libs.error.create( {
                error: data,
                message: 'Could not create user node in database.',
                name: _methodName
              } )
            }

            // This may not be necessary if handled with user exports
            // i.e. Database triggers which set custom user claims on write
            if ( properties.claims ) {
              return admin.auth().setCustomUserClaims( properties.id, properties.claims )

                .catch( error => { return libs.error.admin.auth( error ) } )

                .then( data => {
                  if ( data instanceof Error ) {
                    return libs.error.create( {
                      error: data,
                      message: 'Could not set claims for user.',
                      name: _methodName
                    } )
                  } else {
                    return properties
                  }
                } )
            } else {
              return properties
            }
          } )
      }

      _user.create.database = userCreateDatabase

      /*
       * User References
       * module.exports.user.reference
       * Handles references for a user.
       * References are represented as IDs in user properties which reference other database objects.
       * References are indexed by GUIDs resulting from a `push` to the database.
       * i.e. `user/{uid}/invoice/{guid}` = `{id}`, referencing `invoice/{id}`
       */

      _user.reference = {}

      /** @function user.reference.fetch
       * Fetch a reference for a given or authorized user.
       * @param {Object} params - The parameter object.
       * @param {string} params.key - The key the property is indexed at.
       * @param {string} params.uid - (Optional) The user id. Defaults to user authorized from bearer token.
       * @returns {Promise} The value of the node that is being referenced, or an error.
       */
      let userReferenceFetch = ( params = {} ) => {
        let _methodName = 'user-reference-fetch'
        let key = params.key

        return userPropertyFetch( params )

          .then( dataSnapshot => {
            if ( dataSnapshot instanceof Error ) {
              return libs.error.create( {
                error: dataSnapshot,
                message: 'Could not resolve user reference.',
                name: _methodName
              } )
            } else {
              return libs.database.reference.resolve( dataSnapshot )
            }
          } )

          .then( data => {
            let body = {}
            let value

            if ( data instanceof Error ) {
              return libs.error.create( {
                error: data,
                message: 'Could not resolve reference for user property.',
                name: _methodName
              } )

            // May be an array as a result of a Promise.all()
            // retrieval of each indexed reference
            } else if ( data.constructor === Array ) {
              value = []

              for ( let dataSnapshot of data ) {
                value.push( dataSnapshot.val() )
              }
            } else {
              value = data.val()
            }

            body[ key ] = value

            return data
          } )
      }

      _user.reference.fetch = userReferenceFetch

      /*
       * User Properties
       * module.exports.user.property
       * Properties are the represented as child nodes for a given user.
       * i.e. `user/{uid}/{property}
       */

      _user.property = {}

      /** @function user.property.fetch
       * Fetch a property for a given or authorized user.
       * @param {Object} params - The parameter object.
       * @param {string} params.key - The key the property is indexed at.
       * @param {string} params.uid - (Optional) The user id. Defaults to user authorized from bearer token.
       * @returns {Promise} A `dataSnapshot` of a user's node, or an error.
       */
      let userPropertyFetch = ( params = {} ) => {
        let _methodName = 'user-property-fetch'
        let key = params.key

        if ( params.uid ) {
          let userPath = libs.user.database.path( params.uid )

          return libs.database.fetch( {
            path: `${userPath}/${key}`
          } )

            .then( dataSnapshot => {
              if ( dataSnapshot instanceof Error ) {
                return libs.error.create( {
                  error: dataSnapshot,
                  message: 'Could not fetch user property.',
                  name: _methodName
                } )
              }

              return dataSnapshot
            } )
        } else {
          return authorizeBearer()

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  message: 'Could not fetch user property.',
                  name: _methodName
                } )
              } else {
                params.uid = data.decodedIdToken.uid

                return userPropertyFetch( params )
              }
            } )
        }
      }

      _user.property.fetch = userPropertyFetch

      /** @function user.property.set
       * Set a property for a given or authorized user.
       * @param {Object} params - The parameter object.
       * @param {string} params.key - The key the property is indexed at.
       * @param {*} params.value - The value of the property to be indexed.
       * @param {string} params.uid - (Optional) The user id. Defaults to user authorized from bearer token.
       * @returns {Promise} A null value or an error.
       */
      let userPropertySet = ( params = {} ) => {
        let _methodName = 'user-property-set'
        let key = params.key

        if ( params.uid ) {
          let userPath = libs.user.database.path( params.uid )

          return libs.database.set( {
            path: `${userPath}/${key}`,
            value: params.value
          } )

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  message: 'Could not set user property.',
                  name: _methodName
                } )
              }
            } )
        } else {
          return authorizeBearer()

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  message: 'Could not set user property.',
                  name: _methodName
                } )
              } else {
                params.uid = data.decodedIdToken.uid

                return userPropertySet( params )
              }
            } )
        }
      }

      _user.property.set = userPropertySet

      /** @function user.property.push
       * Push a property for a given or authorized user.
       * @param {Object} params - The parameter object.
       * @param {string} params.key - The key the property is indexed at.
       * @param {*} params.value - The value of the property to be indexed.
       * @param {string} params.uid - (Optional) The user id. Defaults to user authorized from bearer token.
       * @returns {Promise} A null value or an error.
       */
      let userPropertyPush = ( params = {} ) => {
        let _methodName = 'user-property-push'

        if ( params.uid ) {
          let userPath = libs.user.database.path( params.uid )

          return libs.database.push( {
            path: `${userPath}/${params.key}`,
            value: params.value
          } )

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  message: 'Could not push user property.',
                  name: _methodName
                } )
              }

              return data
            } )
        } else {
          return authorizeBearer()

            .then( data => {
              if ( data instanceof Error ) {
                return libs.error.create( {
                  error: data,
                  message: 'Could not push user property.',
                  name: _methodName
                } )
              } else {
                params.uid = data.decodedIdToken.uid

                return userPropertyPush( params )
              }
            } )
        }
      }

      _user.property.push = userPropertyPush

      /*
       * Utility
       * @private
       */

      /** @function verifyToken
       * @private
       * Verify token with Firebase Auth via the Admin SDK.
       * @param {Object} params - The parameter object.
       * @param {string} params.token - A Firebase Auth user token or a Firebase Auth Custom token.
       * @param {Boolean} params.forceRefresh - Force refresh regardless of token expiration.
       * @returns {Promise} A `decodedIdToken` for a Firebase Auth user or an error.
       */
      let verifyToken = ( params = {} ) => {
        let _methodName = 'verify-token'

        return admin.auth().verifyIdToken( params.token )

          .then( decodedIdToken => {
            return {
              decodedIdToken: decodedIdToken,
              idToken: params.token
            }
          } )

          .catch( adminError => {
            adminError = libs.error.admin.auth( adminError )

            // only attempt admin sign in
            if ( settings.customToken !== false ) {
              return firebase.auth().signInWithCustomToken( params.token )

              // render `decodedIdToken from `user` to standardize return value
                .then( user => {
                  // force refresh may be null
                  return user.getIdToken( params.forceRefresh )

                    .then( idToken => {
                      // replace custom token in `params` with `idToken`
                      params.token = idToken

                      // do not retry custom token sign in
                      settings.customToken = false

                      // recursively fire function to decode newly generated token
                      return verifyToken( params )
                    } )
                } )

                .catch( firebaseError => {
                  firebaseError = libs.error.firebase.auth( firebaseError )

                  return libs.error.create( {
                    error: new VError.MultiError( [ adminError, firebaseError ] ),
                    message: 'Could not verify auth user id token.',
                    name: _methodName
                  } )
                } )
            }

            return libs.error.create( {
              error: adminError,
              message: 'Could not verify auth user id token.',
              name: _methodName
            } )
          } )
      }

      /** @function bearerToken
       * @private
       * Retrieve bearer token from 'Authorization' header
       * @param {Object} params - The parameter object.
       * @param {string} params.bearer - (Optional) A string with the token. i.e. 'Bearer <token>'
       * @returns {string} The id token or an error.
       */
      let bearerToken = ( params = {} ) => {
        let _methodName = 'bearer-token'
        let header = 'Authorization'
        let bearer = params.bearer || request.get( header )

        try {
          return bearer.split( 'Bearer ' )[ 1 ]
        } catch ( error ) {
          libs.log( 'Invalid Authorization header:', bearer )

          return libs.error.create( {
            error: error,
            info: {
              https: {
                code: 401,
                response: {
                  message: 'Invalid bearer token.'
                }
              }
            },
            message: 'Invalid or missing bearer token.',
            name: _methodName
          } )
        }
      }

      return {
        // Trigger libraries
        authorize: _authorize,
        end: _end,
        response: _response,
        user: _user,
        project: _project,
        // Local libraries
        libs: libs,
        // Initialized packages
        package: {
          admin: admin,
          firebase: firebase,
          pubsub: pubsub,
          storage: storage
        }
      }
    }
  }
}
