const moment = require( 'moment' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    return ( data, eventContext ) => {
      // Local libraries
      const libs = context.libs

      // Initialized node packages
      // `firebase-admin`
      const admin = context.admin
      // `firebase`
      const firebase = context.firebase
      // `@google-cloud/pubsub`
      const pubsub = context.pubsub
      // `@google-cloud/storage`
      const storage = context.storage

      /*
       * Meta
       * #meta
       * module.meta
       */

      let _meta = {}

      let metaCreated = () => {
        // `onCreate` function
        let snap = data
        if ( eventContext.params.id !== 'meta' ) {
          return libs.database.set( {
            path: libs.database.path( { ref: snap.ref } ) + '/meta/created',
            value: moment().format()
          } )
            .then( data => {
              if ( data instanceof Error ) { console.error( 'Could not write to database:', data ) }
              return data
            } )
        } else {
          return snap
        }
      }
      _meta.created = metaCreated

      let metaUpdated = () => {
        // `onWrite` function
        let change = data
        if ( eventContext.params.id !== 'meta' && eventContext.params.field !== 'meta' ) {
          return change.after.ref.parent.once( 'value' )
            .then( dataSnapshot => {
              if ( dataSnapshot.val() !== null ) {
                return libs.database.set( {
                  path: libs.database.path( { ref: dataSnapshot.ref } ) + '/meta/updated',
                  value: moment().format()
                } )
                  .then( data => {
                    if ( data instanceof Error ) { console.error( 'Could not write to database:', data ) }
                    return data
                  } )
              } else {
                return change
              }
            } )
        } else {
          return change
        }
      }
      _meta.updated = metaUpdated

      /*
       * Project
       * #project
       * module.project
       */

      let _project = {}

      /** @function project.initialize
       * Instantiates this Database Trigger library for a given project.
       * This involves passing service account credentials along with the project id to the necessary dependencies.
       * @param {Object} params - The parameter object.
       * @param {string} params.projectId - The id for the project you wish to act on behalf of.
       * @param {Object} params.serviceAccount - (Optional) JSON formatted service account data. Will default to a service account passed in when loading the main library.
       * @returns {Object} An instance of this library initialized for a specific project.
       */
      let initializeProject = ( params = {} ) => {
        // let _methodName = 'project-initialize'
        let projectContext = libs.project.context.initialize( {
          context: context,
          projectId: params.projectId,
          serviceAccount: params.serviceAccount || context.serviceAccount
        } )

        return require( './index' )( projectContext )( data, eventContext )
      }
      _project.initialize = initializeProject

      /*
       * User
       * module.user
       */

      let _user = {}

      /*
       * User Claims
       * module.exports.user.claims
       */

      _user.claims = {}

      let setUserClaims = ( params = {} ) => {
        return admin.auth().setCustomUserClaims( params.uid, params.claims ).catch( libs.error.admin.auth )
      }
      _user.claims.set = setUserClaims

      return {
        // Trigger libraries
        meta: _meta,
        project: _project,
        user: _user,
        // Local Libraries
        libs: libs,
        // Initialized packages
        package: {
          admin: admin,
          firebase: firebase,
          pubsub: pubsub,
          storage: storage
        }
      }
    }
  }
}
