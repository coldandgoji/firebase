module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    return {
      analytics: require( './analytics/index' )( context )( settings ),
      auth: require( './auth/index' )( context )( settings ),
      crashlytics: require( './crashlytics/index' )( context )( settings ),
      database: require( './database/index' )( context )( settings ),
      firestore: require( './firestore/index' )( context )( settings ),
      https: require( './https/index' )( context )( settings ),
      pubsub: require( './pubsub/index' )( context )( settings ),
      storage: require( './storage/index' )( context )( settings )
    }
  }
}
