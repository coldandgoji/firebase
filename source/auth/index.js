module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    return ( data, eventContext ) => {
      // Local libraries
      const libs = context.libs

      // Initialized node packages
      // `firebase-admin`
      const admin = context.admin
      // `firebase`
      const firebase = context.firebase
      // `@google-cloud/pubsub`
      const pubsub = context.pubsub
      // `@google-cloud/storage`
      const storage = context.storage

      /*
       * Project
       * #project
       */

      let _project = {}

      /** @function project.initialize
       * Instantiates this Auth Trigger library for a given project.
       * This involves passing service account credentials along with the project id to the necessary dependencies.
       * @param {Object} params - The parameter object.
       * @param {string} params.projectId - The id for the project you wish to act on behalf of.
       * @param {Object} params.serviceAccount - (Optional) JSON formatted service account data. Will default to a service account passed in when loading the main library.
       */
      let initializeProject = ( params = {} ) => {
        // let _methodName = 'project-initialize'
        let projectContext = libs.project.context.initialize( {
          context: context,
          projectId: params.projectId,
          serviceAccount: params.serviceAccount || context.serviceAccount
        } )

        return require( './index' )( projectContext )( data, eventContext )
      }
      _project.initialize = initializeProject

      return {
        // Trigger libraries
        project: _project,
        // Local Libraries
        libs: libs,
        // Initialized packages
        package: {
          admin: admin,
          firebase: firebase,
          pubsub: pubsub,
          storage: storage
        }
      }
    }
  }
}
