# @coldandgoji/firebase

## Versions

Stable: `1.0.0`

## Setup

### Installation

It is recommended that you peg to a stable version and not defer to the latest release.

```
npm install --save @coldandgoji/firebase@1.0.0
```

### Require
```javascript
let context = {
  functions: require( 'firebase-functions' ),
  serviceAccount: require( './credentials/serviceAccount.json' )
}
let settings = {}
let core = require( '@coldandgoji/firebase' )( context )( settings )
```
#### `serviceAccount`

Download your service account credentials in JSON format. You can review [the Google Cloud IAM documentation](https://cloud.google.com/iam/docs/creating-managing-service-account-keys) on how to do this.

## Usage

You can access either the general libraries or the trigger specific libraries.

### General Libraries

These libraries are indexed in two places.
- `core.libs`
- `core.trigger[ triggerName ]().libs` (see Trigger Specific Libraries section) 


### Trigger Specific Libraries

There is a set of libraries for each of the Firebase Functions triggers. For example, to expose the Storage libraries when writing a cloud function:

```javascript
exports.sampleStorage = functions.storage.bucket( bucketName ).object( 'sample' ).onFinalize( ( object, eventContext ) => {
  let triggerLibrary = core.triggers.storage( object, eventContext )
} )
```

The function arguments should be passed into the trigger libraries, as above. For HTTPS functions, this would be:

```javascript
exports.sample = functions.https.onRequest( ( request, response, next ) => {
  let triggerLibrary = core.triggers.https( request, response, next )
} )
```

## Support

Open up a [new issue](https://gitlab.com/coldandgoji/firebase/issues/new) if you run into any problems.
