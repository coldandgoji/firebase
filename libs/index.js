const _ = require( 'lodash' )
const adminAPI = require( 'firebase-admin' )
const firebaseAPI = require( 'firebase' )
const jsonwebtoken = require( 'jsonwebtoken' )
const pubsubAPI = require( '@google-cloud/pubsub' )
const storageAPI = require( '@google-cloud/storage' )
const stringify = require( 'json-stringify-safe' )
const uuid = require( 'uuid/v4' )
const VError = require( 'verror' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    // Environment variables
    let env = context.env
    let config = context.functions.config()

    // Initialized node packages
    // `firebase-admin`
    let admin = context.admin
    // `firebase`
    // let firebase = context.firebase
    // `@google-cloud/pubsub`
    // let pubsub = context.pubsub
    // `@google-cloud/storage`
    // let storage = context.storage

    /*
     * Admin
     * #admin
     */

    let _admin = {}

    /** @function admin.initialize
     * Initializes the `firebase-admin` (Admin SDK) library for a given project.
     * @param {Object} params - The parameter object.
     * @param {string} params.projectId - The id for the project you wish to act on behalf of.
     * @param {Object} params.serviceAccount - (Optional) JSON formatted service account data. Will default to a service account passed in when loading the main library.
     * @returns {Object} An initialized `firebase-admin` library.
     */
    let initializeAdmin = ( params = {} ) => {
      let serviceAccount = params.serviceAccount || context.serviceAccount

      if ( serviceAccount ) {
        let projectId = params.projectId || serviceAccount.project_id

        return adminAPI.initializeApp( {
          credential: adminAPI.credential.cert( serviceAccount ),
          databaseURL: databaseURL( { projectId: projectId } ),
          storageBucket: storageBucket( { projectId: projectId } ),
          projectId: projectId
        }, uuid() )
      } else {
        return adminAPI.initializeApp( firebaseConfig(), uuid() )
      }
    }
    _admin.initialize = initializeAdmin

    /*
     * Auth
     * #auth
     */

    let _auth = {}

    let deleteAuthUser = uid => {
      _log( 'Deleting Firebase Auth User...', uid )
      return admin.auth().deleteUser( uid ).catch( adminAuthError )
    }
    _auth.delete = deleteAuthUser

    let getAuthUser = ( params = {} ) => {
      _log( 'Getting Firebase Auth User...', params )
      if ( params.email ) {
        return admin.auth().getUserByEmail( params.email ).catch( adminAuthError )
      } else if ( params.phone ) {
        return admin.auth().getUserByPhoneNumber( params.phone ).catch( adminAuthError )
      } else {
        return admin.auth().getUser( params.uid ).catch( adminAuthError )
      }
    }
    _auth.get = getAuthUser

    let listAuthUsers = ( params = {} ) => {
      _log( 'Listing Firebase Auth Users...', params )
      return admin.auth().listUsers( params.maxResults || 1000, params.pageToken ).catch( adminAuthError )
    }
    _auth.list = listAuthUsers

    let listAllAuthUsers = ( params = {} ) => {
      let users = params.users || []

      return listAuthUsers( params )
        .then( result => {
          if ( result instanceof Error ) { return result }
          users = _.concat( users, result.users )
          if ( result.pageToken ) {
            return listAllAuthUsers( { users: users, pageToken: result.pageToken } )
          } else {
            return users
          }
        } )
    }
    _auth.list.all = listAllAuthUsers

    let updateAuthUser = ( params = {} ) => {
      _log( 'Updating Firebase Auth User...', params )
      return admin.auth().updateUser( params.uid, params.properties ).catch( adminAuthError )
    }
    _auth.update = updateAuthUser

    let authDomain = ( params = {} ) => { return `${params.projectId || projectId()}.firebaseapp.com` }
    _auth.domain = authDomain

    /*
     * Auth Claims
     * #auth-claims
     */

    _auth.claims = {}

    let setAuthClaims = ( params = {} ) => {
      return admin.auth().setCustomUserClaims( params.uid, params.claims ).catch( adminAuthError )
    }
    _auth.claims.set = setAuthClaims

    /*
     * Auth Display Name
     * #auth-displayName
     */

    _auth.displayName = {}

    /** @function auth.displayName.format
     * Returns appropriate `displayName` for user.
     * @param {Object} params - The parameter object.
     * @param {string} params.firstname - (Optional) The firstname of the user. Will only return `params.lastname` if omitted.
     * @param {string} params.lastname - (Optional) The lastname of the user. Will only return `params.firstname` if omitted.
     * @returns {string|null} A single name for the user.
     */
    let formatAuthDisplayName = ( params = {} ) => {
      let firstname = params.firstname
      let lastname = params.lastname

      if ( firstname && lastname ) {
        return `${firstname} ${lastname}`
      } else if ( firstname ) {
        return firstname
      } else if ( lastname ) {
        return lastname
      } else {
        return ''
      }
    }
    _auth.displayName.format = formatAuthDisplayName

    /*
     * Cart
     * #cart
     */

    let _cart = {}

    let fetchCart = uid => { return databaseFetch( { path: cartPath( uid ) } ) }
    _cart.fetch = fetchCart

    /*
     * Cart Database
     * #cart-database
     */

    _cart.database = {}

    let cartPath = id => { return `cart/${id}` }
    _cart.database.path = cartPath

    /*
     * Cart Item
     * #cart-item
     */

    _cart.item = {}

    let cartItemPrice = ( params = {} ) => {
      return _.floor( productCurrentPrice( { product: params.item.product } ), 2 )
    }
    _cart.item.price = cartItemPrice

    /*
     * Cart Item Product
     * #cart-item-product
     */

    _cart.item.product = {}

    let mapCartItemProducts = ( params = {} ) => {
      /*
       * Accepts: `item`
       * { productId: '' }
       *
       * Returns: `item` with:
       * { product: {} }
       *
       * Also, recursively maps products for `item.options`
       */

      let item = params.item
      let addons = item.addons || {}
      let promises = []

      return databaseFetch( { path: `content/product/${item.productId}` } )
        .then( productSnapshot => {
          item.product = productSnapshot.val()

          // Map products for the item's options
          _.forIn( addons, ( addon, addonId ) => {
            promises.push( mapCartItemProducts( { item: addon } )
              // Replace `option` with mapped `option`
              .then( data => { addons[ addonId ] = data } ) )
          } )

          // If the item has options, map their products
          if ( promises.length > 0 ) {
            return Promise.all( promises ).catch( error => { return error } )
              .then( data => {
                data = parseError( data )
                if ( data instanceof Error ) { return data }
                // Replace old `addons` with newly mapped `addons`
                item.addons = addons
                return item
              } )
          // No options, return item as is
          } else {
            return item
          }
        } )
    }
    _cart.item.product.map = mapCartItemProducts

    /*
     * Cart Items
     * #cart-items
     */

    _cart.items = {}

    let fetchCartItems = uid => {
      return databaseFetch( { path: cartPath( uid ) } )
        .then( cartSnapshot => {
          if ( cartSnapshot instanceof Error ) { return cartSnapshot }
          return mapCartItemsProducts( { items: cartSnapshot.child( 'items' ).val() } )
        } )
    }
    _cart.items.fetch = fetchCartItems

    let validateCartItemsCost = ( params = {} ) => {
      _log( 'Validating cart...', params )

      // let errorName = 'validate-cart'
      let cost = params.cost
      let fetchedItems

      return fetchCartItems( params.uid )

        .then( items => {
          fetchedItems = items
          return formatInvoiceItems( { items: items } )
        } )

        .then( items => {
          let total = cartItemsPriceTotal( { items: items } )

          if ( _.floor( total.amount, 2 ) !== _.floor( cost.amount, 2 ) ) {
            _log( 'Calculated Cart amount of', total.amount, 'does not match Checkout Cart amount of', cost.amount )
            return createError( {
              info: {
                https: {
                  code: 400,
                  response: {
                    message: 'Declared cost of cart items is incorrect.',
                    status: 'cart/invalid-cost'
                  }
                }
              },
              message: 'Could not verify cart cost.'
            } )
          } else if ( cost.currency !== currency() ) {
            _log( 'Calculated Cart currency of', currency(), 'does not match Checkout Cart currency of', cost.currency )
            return createError( {
              info: {
                https: {
                  code: 400,
                  response: {
                    message: 'Declared currency is incorrect.',
                    status: 'cart/invalid-currency'
                  }
                }
              },
              message: 'Could not verify cart cost.'
            } )
          }

          return {
            fetchedItems: fetchedItems,
            formattedItems: items,
            total: total
          }
        } )
    }
    _cart.items.validateCost = validateCartItemsCost

    /*
     * Cart Items Price
     * #cart-items-price
     */

    _cart.items.price = {}

    let cartItemsPriceTotal = ( params = {} ) => {
      let items = params.items
      let amount = 0

      _.each( items, item => {
        amount += ( item.price * item.quantity )

        let addons = item.addons || {}
        _.each( addons, addon => { amount += ( addon.price * item.quantity ) } )
      } )

      return {
        amount: _.floor( amount, 2 ),
        currency: currency()
      }
    }
    _cart.items.price.total = cartItemsPriceTotal

    /*
     * Cart Items Product
     * #cart-items-product
     */

    _cart.items.product = {}

    let mapCartItemsProducts = ( params = {} ) => {
      _log( 'Mapping products to items...', params )

      let items = params.items
      let promises = []

      _.forIn( items, ( item, itemId ) => {
        promises.push(
          mapCartItemProducts( { item: item } )
            .then( data => { items[ itemId ] = data } )
        )
      } )

      return Promise.all( promises )
        .catch( error => { return error } )
        .then( data => {
          if ( data instanceof Error ) { return data }
          return items
        } )
    }
    _cart.items.product.map = mapCartItemsProducts

    /*
     * Database
     * #database
     */

    let _database = {}

    /** @function database.format
     * @private
     * Formats a value to be added to the database. Null values are removed to avoid unneccesary errors.
     * @param {Object} params - The parameter object.
     * @param {string} params.value - The value to be formatted for the database.
     * @returns {*} A value that is valid for the Firebase Realtime Database.
     */
    let databaseFormat = ( params = {} ) => {
      // let _methodName = 'database-format'
      let value = params.value
      let formatted

      if ( _.isNil( value ) ) { return '' }

      switch ( value.constructor ) {
        case Array:

          formatted = []

          _.each( value, entry => {
            formatted.push( databaseFormat( { value: entry } ) )
          } )

          return formatted

        case Object:

          formatted = {}

          _.forIn( value, ( data, key ) => {
            formatted[ key ] = databaseFormat( { value: data } )
          } )

          return formatted

        case Function:

          return stringify( value )

        case Number:

          if ( _.isNaN( value ) ) { return _.toString( value ) }

          return value

        case Boolean:

          return value

        default:

          return _.toString( value )
      }
    }
    _database.format = databaseFormat

    /** @function database.fetch
     * Gets the value for a node.
     * @param {Object} params - The parameter object.
     * @param {string} params.path - A path to a database node.
     * @returns {Promise} A Firebase Database `dataSnapshot` for a node or an error.
     */
    let databaseFetch = ( params = {} ) => {
      let _methodName = 'database-fetch'

      _log( 'Fetching from database:', params )

      return admin.database()
        .ref( params.path )
        .once( 'value' )
        .catch( error => {
          return createError( {
            error: error,
            message: `Could not fetch from database: ${params.path}`,
            name: _methodName,
            https: { code: 400 },
            info: {
              package: 'firebase-admin',
              function: {
                database: null,
                ref: params.path,
                once: 'value'
              }
            }
          } )
        } )
    }
    _database.fetch = databaseFetch

    /** @function database.update
     * Updates the values of child nodes.
     * @param {Object} params - The parameter object.
     * @param {string} params.path - A path to a database node.
     * @param {Object} params.update - The paths and values to be updated. Keys in the object are treated as child paths from the reference at `params.path`.
     * @returns {Promise} A void response for success or an error.
     */
    let databaseUpdate = ( params = {} ) => {
      let _methodName = 'database-update'

      _log( 'Updating database:', params )

      return admin.database()
        .ref( params.path )
        .update( params.update )
        .catch( error => {
          return createError( {
            error: error,
            message: `Could not update database: ${params.path}`,
            name: _methodName,
            https: { code: 400 },
            info: {
              package: 'firebase-admin',
              function: {
                database: null,
                ref: params.path,
                update: params.update
              }
            }
          } )
        } )
    }
    _database.update = databaseUpdate

    /** @function database.key
     * Generates a new key for a node, executing an empty `push`.
     * @param {Object} params - The parameter object.
     * @param {string} params.path - A path to a database node.
     * @returns {Promise} A string representing the key of a node or an error.
     */
    let databaseKey = ( params = {} ) => {
      let _methodName = 'database-key'

      _log( 'Generating key from database:', params )

      return admin.database()
        .ref( params.path )
        .push()
        .catch( error => {
          return createError( {
            error: error,
            message: `Could not push to database: ${params.path}`,
            name: _methodName,
            https: { code: 400 },
            info: {
              package: 'firebase-admin',
              function: {
                database: null,
                ref: params.path,
                push: null
              }
            }
          } )
        } )

        .then( reference => {
          if ( reference instanceof Error ) { return reference }

          return reference.key
        } )
    }
    _database.key = databaseKey

    /** @function database.push
     * Pushes a value to a node.
     * @param {Object} params - The parameter object.
     * @param {string} params.path - A path to a database node.
     * @param {*} params.value - The value to be pushed.
     * @returns {Reference|Error} A database reference for the node or an error.
     */
    let databasePush = ( params = {} ) => {
      let _methodName = 'database-push'

      let value = databaseFormat( { value: params.value } )

      _log( 'Pushing to database:', { path: params.path, value: value } )

      return admin.database()
        .ref( params.path )
        .push( value )
        .catch( error => {
          return createError( {
            error: error,
            message: `Could not push to database: ${params.path}`,
            name: _methodName,
            https: { code: 400 },
            info: {
              package: 'firebase-admin',
              function: {
                database: null,
                ref: params.path,
                push: value
              }
            }
          } )
        } )
    }
    _database.push = databasePush

    /** @function database.set
     * Sets a value to a node.
     * @param {Object} params - The parameter object.
     * @param {string} params.path - A path to a database node.
     * @param {*} params.value - The value to be pushed.
     * @returns {Promise} A void response for success or an error.
     */
    let databaseSet = ( params = {} ) => {
      let _methodName = 'database-set'

      let value = databaseFormat( { value: params.value } )

      _log( 'Setting to database:', { path: params.path, value: value } )

      return admin.database()
        .ref( params.path )
        .set( value )
        .catch( error => {
          return createError( {
            error: error,
            message: `Could not set to database: ${params.path}`,
            name: _methodName,
            https: { code: 400 },
            info: {
              package: 'firebase-admin',
              function: {
                database: null,
                ref: params.path,
                set: value
              }
            }
          } )
        } )
    }
    _database.set = databaseSet

    /** @function database.transaction
     * Performs a transaction function on a node.
     * @param {Object} params - The parameter object.
     * @param {string} params.path - A path to a database node.
     * @param {function} params.func - A function to be performed on the reference.
     * @returns {Promise} A boolean with the committed state of the transaction and a `dataSnapshot` of the node or an error.
     */
    let databaseTransaction = ( params = {} ) => {
      let _methodName = 'database-transaction'

      _log( 'Transacting with database:', params )

      return admin.database()
        .ref( params.path )
        .transaction( params.func )
        .catch( error => {
          return createError( {
            error: error,
            message: `Could not transact with database: ${params.path}`,
            name: _methodName,
            https: { code: 400 },
            info: {
              package: 'firebase-admin',
              function: {
                database: null,
                ref: params.path,
                transaction: params.func
              }
            }
          } )
        } )
    }
    _database.transaction = databaseTransaction

    /** @function database.remove
     * Removes the value of a node.
     * @param {Object} params - The parameter object.
     * @param {DatabaseReference} params.ref - A Firebase Database reference.
     * @returns {Promise} A void response for success or an error.
     */
    let databaseRemove = ( params = {} ) => {
      let _methodName = 'database-remove'

      _log( 'Removing from database:', params )

      return admin.database()
        .ref( params.path )
        .remove()
        .catch( error => {
          return createError( {
            error: error,
            message: `Could not remove from database: ${params.path}`,
            name: _methodName,
            https: { code: 400 },
            info: {
              package: 'firebase-admin',
              function: {
                database: null,
                ref: params.path,
                remove: null
              }
            }
          } )
        } )
    }
    _database.remove = databaseRemove

    /** @function database.url
     * Provides the Firebase Database URL for a given project.
     * @param {Object} params - The parameter object.
     * @param {string} params.projectId - The id for the project.
     * @returns {string} The url for a project's Firebase Realtime Database.
     */
    let databaseURL = ( params = {} ) => { return `https://${params.projectId || projectId()}.firebaseio.com` }
    _database.url = databaseURL

    let databasePath = ( params = {} ) => {
      let path = ''
      let ref = params.ref
      while ( ref.key !== null ) {
        path = `/${ref.key}${path}`
        ref = ref.parent
      }
      return path
    }
    _database.path = databasePath

    /*
     * Database Reference
     * #database-reference
     */

    _database.reference = {}

    /** @function database.reference.resolve
     * Resolve references for one or many ids.
     * These ids are indexed at the same key from the root of the database.
     * i.e. /user/{uid}/invoice/{id}, references => /invoice/{id}
     * @param {DataSnapshot} dataSnapshot - A Firebase Database dataSnapshot.
     * @returns {Promise} Contains a `dataSnapshot` or an error or an array of any combination of the two, depending on the scope of the request.
     */
    let resolveDatabaseReference = ( dataSnapshot = {} ) => {
      // let _methodName = 'database-reference-resolve'
      let key = dataSnapshot.key
      let value = dataSnapshot.val()
      let promises = []

      if ( value.constructor === String ) {
        return fetch( { path: `${key}/${value}` } )
      } else if ( value.constructor === Array ) {
        for ( let item of value ) {
          promises.push( fetch( { path: `${key}/${item}` } ) )
        }

        return Promise.all( promises )
      } else {
        for ( let guid in value ) {
          promises.push( fetch( { path: `${key}/${value[ guid ]}` } ) )
        }

        return Promise.all( promises )
      }
    }
    _database.reference.resolve = resolveDatabaseReference

    /*
     * Error
     * #error
     */

    let _error = {}

    /** @function error.create
     * Renders new VError with proper formatting of error properties.
     * @param {Object} params - Parameter objects.
     * @param {VError} params.error - VError which is the cause of the new error.
     * @param {Object} params.info - (Optional) VError info for new error.
     * @param {String} params.message - (Optional) Message for new error. Defaults to HTTPS response body message.
     * @param {String} params.name - (Optional) Name for new error. Defaults to HTTPS response body status.
     */
    let createError = ( params = {} ) => {
      // https://github.com/joyent/node-verror#constructors

      let error
      // let errors = []

      if ( params.error instanceof VError.MultiError ) {
        error = extractDominantError( params.error )
        // errors = extractErrorMessages( error )
      } else {
        error = params.error
      }

      let options = {}

      // Generate `options` for VError
      options.cause = error
      options.info = createErrorInfo( { error: error, info: params.info } )
      options.name = params.name || options.info.https.response.status

      // Use provided message
      // Default to HTTPS response body message
      let message = params.message || options.info.https.response.message

      return new VError( options, message )
    }
    _error.create = createError

    let parseError = data => {
      let errors = []

      if ( data instanceof Error ) { return data }

      if ( _.isArray( data ) ) {
        _.each( data, item => { if ( item instanceof Error ) { errors.push( item ) } } )
      }

      if ( errors.length > 1 ) {
        return new VError.MultiError( errors )
      } else if ( errors.length ) {
        return errors[ 0 ]
      } else {
        return data
      }
    }
    _error.parse = parseError

    // Private
    let extractDominantError = multiError => {
      let _error

      // Pull dominant error from MultiError
      VError.errorForEach( multiError, error => {
        if ( !_error ) { _error = error }

        let _info = VError.info( _error ) || {}
        let _https = _info.https || {}
        let _code = _https.code || 0

        let info = VError.info( error ) || {}
        let https = info.https || {}
        let code = https.code || 0

        if ( code > _code ) { _error = error }
      } )

      return _error
    }

    // Private
    /* let extractErrorMessages = error => {
      let messages = []

      // VError.errorFromList( multiError, error => { messages.push( error.message ); } );
      if ( error instanceof VError.MultiError ) {
        VError.errorForEach( error, e => {
          messages.push( e.message )
        } )
      }

      return messages
    } */

    // Private
    let createErrorInfo = ( params = {} ) => {
      let error = params.error

      // Use overriding `info`, if provided
      // Otherwise, yield `info` from error
      let info = params.info || VError.info( error ) || {}

      // Fill in any necessary gaps in error info
      info.https = info.https || {}
      info.https.response = info.https.response || {}

      // `info.https` must have
      //   - code: HTTP status code
      //   - response: Object representing response body
      //     - message: String
      //     - status: String

      // Default value for error is 500
      info.https.code = info.https.code || 500

      switch ( info.https.code ) {
        // Bad Request
        case 400:
          info.https.response.message = info.https.response.message || 'Invalid request.'
          info.https.response.status = info.https.response.status || 'invalid-request'
          break

          // Unauthorized
        case 401:
          info.https.response.message = info.https.response.message || 'Unauthorized.'
          info.https.response.status = info.https.response.status || 'unauthorized'
          break

          // Forbidden
        case 403:
          info.https.response.message = info.https.response.message || 'Forbidden.'
          info.https.response.status = info.https.response.status || 'forbidden'
          break

          // Not Found
        case 404:
          info.https.response.message = info.https.response.message || 'Not found.'
          info.https.response.status = info.https.response.status || 'not-found'
          break

          // Internal Server Error
        case 500:
          info.https.response.message = info.https.response.message || 'Internal server error.'
          info.https.response.status = info.https.response.status || 'server-error'
          break

        default:
          info.https.response.message = info.https.response.message || 'Internal server error.'
          info.https.response.status = info.https.response.status || 'server-error'
          break
      }

      return info
    }

    /*
     * Error Admin
     * #error-admin
     * Errors for `firebase-admin` package.
     */

    _error.admin = {}

    /** @function error.admin.auth
     * Creates formatted errors for those coming from the Firebase Node.js Admin SDK `auth` library.
     * @param {Object} params - (Optional) The parameter object.
     * @param {string} params.code - (Optional) Parameterized short message with essential details.
     * @param {string} params.message - (Optional) Describes the nature of the error.
     */
    let adminAuthError = ( params = {} ) => {
      let message = params.message || 'Firebase authentication error.'
      let https = {
        code: 400,
        response: {
          message: message,
          status: params.code || 'auth/invalid-request'
        }
      }

      // TODO: Add cases for error codes
      switch ( params.code ) {
        case 'auth/invalid-argument':
          break

        case 'auth/invalid-disabled-field':
          break

        case 'auth/invalid-display-name':
          break

        case 'auth/invalid-email-verified':
          break

        case 'auth/invalid-email':
          break

        case 'auth/invalid-password':
          break

        case 'auth/invalid-phone-number':
          break

        case 'auth/invalid-photo-url':
          break

        case 'auth/invalid-uid':
          break

        case 'auth/missing-uid':
          break

        case 'auth/uid-already-exists':
          break

        case 'auth/email-already-exists':
          break

        case 'auth/user-not-found':
          https.code = 404
          break

        case 'auth/operation-not-allowed':
          https.code = 401
          break

        case 'auth/invalid-credential':
          https.code = 401
          break

        case 'auth/phone-number-already-exists':
          break

        case 'auth/project-not-found':
          https.code = 404
          break

        case 'auth/insufficient-permission':
          https.code = 401
          break

        case 'auth/internal-error':
          https.code = 500
          break

        default:
          break
      }

      // Assemble VError options
      let options = { info: {} }
      if ( params instanceof Error ) { options.cause = params }
      options.info.https = https

      return new VError( options, message )
    }
    _error.admin.auth = adminAuthError

    /*
     * Error Firebase
     * #error-firebase
     * Errors for `firebase` package.
     * https://firebase.google.com/docs/reference/node/firebase.auth.Error
     * https://firebase.google.com/docs/reference/node/firebase.auth.Auth
     */

    _error.firebase = {}

    /** @function error.firebase.auth
     * Creates formatted errors for those coming from the Firebase Node.js Client `auth` library.
     * @param {Object} params - (Optional) The parameter object.
     * @param {string} params.code - (Optional) Parameterized short message with essential details.
     * @param {string} params.message - (Optional) Describes the nature of the error.
     */
    let firebaseAuthError = ( params = {} ) => {
      let message = params.message || 'Firebase authentication error.'
      let https = {
        code: 400,
        response: {
          message: message,
          status: params.code || 'auth/invalid-request'
        }
      }

      // TODO: Add cases for error codes
      switch ( params.code ) {
        case 'auth/expired-action-code':
          https.code = 401
          break

        case 'auth/invalid-action-code':
          https.code = 401
          break

        case 'auth/user-disabled':
          break

        case 'auth/user-not-found':
          https.code = 404
          break

        case 'auth/weak-password':
          break

        case 'auth/email-already-in-use':
          break

        case 'auth/invalid-email':
          break

        case 'auth/operation-not-allowed':
          break

        case 'auth/missing-android-pkg-name':
          break

        case 'auth/missing-continue-uri':
          https.code = 401
          break

        case 'auth/missing-ios-bundle-id':
          break

        case 'auth/invalid-continue-uri':
          https.code = 401
          break

        case 'auth/unauthorized-continue-uri':
          https.code = 401
          break

        case 'auth/invalid-persistence-type':
          break

        case 'auth/unsupported-persistence-type':
          break

        case 'auth/account-exists-with-different-credential':
          break

        case 'auth/invalid-credential':
          https.code = 401
          break

        case 'auth/custom-token-mismatch':
          https.code = 401
          break

        case 'auth/invalid-custom-token':
          https.code = 401
          break

        case 'auth/captcha-check-failed':
          https.code = 401
          break

        case 'auth/wrong-password':
          https.code = 401
          break

        case 'auth/invalid-verification-code':
          https.code = 401
          break

        case 'auth/invalid-verification-id':
          https.code = 401
          break

        case 'auth/invalid-phone-number':
          https.code = 401
          break

        case 'auth/missing-phone-number':
          https.code = 401
          break

        case 'auth/quota-exceeded':
          https.code = 500
          break

        case 'auth/app-deleted':
          https.code = 500
          break

        case 'auth/app-not-authorized':
          https.code = 500
          break

        case 'auth/argument-error':
          break

        case 'auth/invalid-api-key':
          https.code = 500
          break

        case 'auth/invalid-user-token':
          https.code = 401
          break

        case 'auth/network-request-failed':
          https.code = 500
          break

        case 'auth/requires-recent-login':
          break

        case 'auth/too-many-requests':
          break

        case 'auth/unauthorized-domain':
          break

        case 'auth/web-storage-unsupported':
          break

        default:
          break
      }

      // Assemble VError options
      let options = { info: {} }
      if ( params instanceof Error ) { options.cause = params }
      options.info.https = https

      return new VError( options, message )
    }
    _error.firebase.auth = firebaseAuthError

    /*
     * Error HTTPS
     * #error.https
     */

    _error.https = {}

    let createHTTPSError = httpsError => {
      let code = 0
      let response
      // let errors = extractErrorMessages( httpsError );

      if ( httpsError instanceof VError.MultiError ) {
        httpsError = extractDominantError( httpsError )
      }

      try {
        // Return the response for the error
        let info = VError.info( httpsError ) || {}
        // console.log( 'Error info:', info );
        let https = info.https || {}
        code = https.code || 0
        response = https.response || {}
      } catch ( error ) {
        console.error( new VError( error, 'Could not extract info from error.' ) )
        // console.log( httpsError );
      }

      // console.log( 'Code:', code );
      // console.log( 'Response:', response );

      // console.log( 'Applying defaults for invalid values...' );

      // Defaults
      if ( code < 400 || code > 599 ) { code = 500 }
      response = response || {}
      response.message = response.message || 'Internal server error.'
      response.status = response.status || 'internal-server-error'

      // Add list of errors to esponse
      // response.errors = response.errors || [];
      // response.errors = response.errors.concat( errors );

      return {
        code: code,
        response: response
      }
    }
    _error.https.create = createHTTPSError

    /*
     * Firebase
     * #firebase
     */

    let _firebase = {}

    let firebaseConfig = () => { return JSON.parse( process.env.FIREBASE_CONFIG ) }
    _firebase.config = firebaseConfig

    let initializeFirebase = ( params = {} ) => {
      let firebaseParams = firebaseConfig()
      let projectId = params.projectId || firebaseParams.projectId

      firebaseParams.apiKey = params.apiKey || _.result( env, 'internal.apikey', '' )
      firebaseParams.authDomain = params.authDomain || authDomain( { projectId: projectId } )
      firebaseParams.databaseURL = params.databaseURL || databaseURL( { projectId: projectId } )
      firebaseParams.storageBucket = params.storageBucket || storageBucket( { projectId: projectId } )
      if ( params.messagingSenderId ) { firebaseParams.messagingSenderId = params.messagingSenderId }

      return firebaseAPI.initializeApp( firebaseParams, uuid() )
    }
    _firebase.initialize = initializeFirebase

    /*
     * Functions
     * #functions
     */

    let _functions = {}

    /** @function functions.url
     * Provides the Firebase Functions URL for a given project. The is currently only one region for functions: "us-central1".
     * @param {Object} params - The parameter object.
     * @param {string} params.projectId - The id for the project.
     * @returns {string} The url for a project's Firebase Functions.
     */
    let functionsURL = ( params = {} ) => {
      return `https://us-central1-${params.projectId}.cloudfunctions.net`
    }
    _functions.url = functionsURL

    /*
     * Invoice
     * #invoice
     */
    let _invoice = {}

    /*
     * Invoice Database
     * #invoice-database
     */

    _invoice.database = {}

    let invoicePath = id => { return `invoice/${id}` }
    _invoice.database.path = invoicePath

    /*
     * Invoice Download
     * #invoice-download
     */

    _invoice.download = {}

    let invoiceDownloadURL = ( params = {} ) => {
      return `https://firebasestorage.googleapis.com/v0/b/${params.projectId}.appspot.com/o/invoice%2F${params.fileName}.pdf?alt=media&token=${params.token}`
    }
    _invoice.download.url = invoiceDownloadURL

    /*
     * Invoice Id
     * #invoice-id
     */

    _invoice.id = {}

    let invoiceAutonumber = () => {
      let value = Number( _.result( settings, 'invoice.id.autonumber', NaN ) )
      if ( !_.isNaN( value ) ) {
        return value
      } else {
        return new VError( `Invalid "invoice.id.autonumber" in settings object. (${value})` )
      }
    }
    _invoice.id.autonumber = invoiceAutonumber

    let invoicePrefix = () => {
      let value = _.result( settings, 'invoice.id.prefix', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "invoice.id.prefix" in settings object. (${value})` )
      }
    }
    _invoice.id.prefix = invoicePrefix

    let createInvoiceId = () => {
      let count

      return databaseTransaction( {
        path: 'invoice/meta/counter',
        func: ( counter = 1 ) => {
          counter += 1
          count = counter
          return counter
        }
      } )
        .then( data => {
          if ( data instanceof Error ) { return data }
          return `${invoicePrefix()}-${invoiceAutonumber() + count}`
        } )
    }
    _invoice.id.create = createInvoiceId

    /*
     * Invoice Items
     * #invoice-items
     * item = {
     *   quantity: 1,
     *   product: { <product> },
     *   price: 100,
     *   ( tax: { type: <type>, cost: <cost> }, )
     *   ( discount: { <id>: <discount> }, )
     *   addons: {
     *     <id>: {
     *       product: { <product> }
     *       quantity: 1,
     *       price: 10
     *     }
     *   }
     * }
     */

    _invoice.items = {}

    let formatInvoiceItems = ( params = {} ) => {
      _log( 'Formatting invoice items...' )

      let items = params.items
      let response = {}

      // Create a list of products to fetch
      // List will contain products for all cart items and their addons
      // This design is to limit redundency in fetching products
      let productIds = []

      _.each( items, item => {
        // Add cart item products to the list
        productIds.push( item.productId )

        // Add "Add-On" products to the list
        _.forIn( item.addons || {}, addon => { productIds.push( addon.productId ) } )
      } )

      productIds = _.uniq( productIds )

      let promises = []
      _.each( productIds, productId => {
        promises.push( databaseFetch( { path: `content/product/${productId}` } ) )
      } )

      // Index fetched products
      let products = {}

      return Promise.all( promises )

        .catch( error => { return error } )

        .then( productSnapshots => {
          productSnapshots = parseError( productSnapshots )
          if ( productSnapshots instanceof Error ) { return productSnapshots }

          // Index fetched products
          _.each( productSnapshots, productSnapshot => {
            products[ productSnapshot.key ] = productSnapshot.val()
          } )

          // Format items and index to response
          _.forIn( items, ( item, itemId ) => {
            let productId = item.productId

            let newItem = {
              addons: {},
              product: products[ productId ],
              quantity: item.quantity
            }
            newItem.price = cartItemPrice( { item: newItem } )
            newItem.totalPrice = newItem.price

            // Format item addons and index to item
            _.forIn( item.addons || {}, ( addon, addonId ) => {
              let newAddon = {
                product: products[ addon.productId ],
                quantity: item.quantity
              }
              newAddon.price = cartItemPrice( { item: newAddon } )
              newItem.totalPrice += newAddon.price

              newItem.addons[ addonId ] = newAddon
            } )

            // Disallow "Add-Ons" from existing as cart items
            let attributes = _.result( newItem, `product.attributes.${locale()}`, [] )
            if ( !attributes.includes( productAddonId() ) ) {
              response[ itemId ] = newItem
            } else {
              _log( 'Invalid "Add On" product found in cart items:', newItem )
            }
          } )

          return response
        } )
    }
    _invoice.items.format = formatInvoiceItems

    /*
     * JWT: JSON Web Token
     * #jwt
     */

    let _jwt = {}

    let jwtSecret = () => { return context.serviceAccount.private_key }
    _jwt.secret = jwtSecret

    /*
     * JWT Sign
     * #jwt-sign
     */

    let signJWT = ( params = {} ) => {
      try {
        return jsonwebtoken.sign( params.payload, jwtSecret() )
      } catch ( error ) {
        return jwtSignError( error )
      }
    }
    _jwt.sign = signJWT

    let jwtSignError = error => {
      return createError( {
        error: error,
        info: {
          https: {
            code: 400,
            response: {
              message: 'Could not sign token.',
              status: 'token/invalid-signature'
            }
          }
        },
        message: 'Could not sign token.'
      } )
    }
    _jwt.sign.error = jwtSignError

    /*
     * JWT Verify
     * #jwt-verify
     */

    let verifyJWT = ( params = {} ) => {
      try {
        return jsonwebtoken.verify( params.token, jwtSecret() )
      } catch ( error ) {
        return jwtVerifyError( error )
      }
    }
    _jwt.verify = verifyJWT

    let jwtVerifyError = error => {
      return createError( {
        error: error,
        info: {
          https: {
            code: 400,
            response: {
              message: 'Could not verify token.',
              status: 'token/could-not-verify'
            }
          }
        },
        message: 'Could not verify token.'
      } )
    }
    _jwt.verify.error = jwtVerifyError

    /*
     * Localization
     * #localization
     */

    let _localization = {}

    let currency = () => {
      let value = _.result( config, 'localization.currency', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "localization.currency" in Firebase Functions config object. (${value})` )
      }
    }
    _localization.currency = currency

    let locale = () => {
      let value = _.result( config, 'localization.locale', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "localization.locale" in Firebase Functions config object. (${value})` )
      }
    }
    _localization.locale = locale

    let timezone = () => {
      let value = _.result( config, 'localization.timezone', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "localization.timezone" in Firebase Functions config object. (${value})` )
      }
    }
    _localization.timezone = timezone

    /*
     * Localization Country
     * #localization-country
     */

    _localization.country = {}

    let countryCode = () => {
      let value = _.result( config, 'localization.country.code', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "localization.country.code" in Firebase Functions config object. (${value})` )
      }
    }
    _localization.country.code = countryCode

    /*
     * Log
     * #log
     */

    let _log = ( ...values ) => {
      if ( settings.logging === true || settings.DEBUG === true ) { console.log( ...values ) }
    }

    /*
     * Product
     * #product
     */

    let _product = {}

    /*
     * Product Attribute
     * #product-attribute
     */

    _product.attribute = {}

    let productAddonId = () => {
      let value = _.result( settings, 'product.attribute.addon', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "product.attribute.addon" in settings object. (${value})` )
      }
    }
    _product.attribute.addonId = productAddonId

    let productOnsaleId = () => {
      let value = _.result( settings, 'product.attribute.onsale', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "product.attribute.onsale" in settings object. (${value})` )
      }
    }
    _product.attribute.onsaleId = productOnsaleId

    /*
     * Product Price
     * #product-price
     */

    _product.price = {}

    let productCurrentPrice = ( params = {} ) => {
      let product = params.product
      let attributesField = product.attributes || {}
      let attributes = attributesField[ locale() ] || []
      let isOnSale = attributes.includes( productOnsaleId() )

      // #fix
      if ( isOnSale ) {
        let salePrice = productSalePrice( { product: product } )
        if ( _.isNaN( salePrice ) ) {
          return productRegularPrice( { product: product } )
        } else {
          return salePrice
        }
      } else {
        return productRegularPrice( { product: product } )
      }
    }
    _product.price.current = productCurrentPrice

    let productRegularPrice = ( params = {} ) => {
      return _.floor( _.get( params, `product.price.${locale()}` ), 2 )
    }
    _product.price.regular = productRegularPrice

    let productSalePrice = ( params = {} ) => {
      return _.floor( _.get( params, `product.salePrice.${locale()}` ), 2 )
    }
    _product.price.sale = productSalePrice

    /*
     * Project
     * #project
     */

    let _project = {}

    let projectId = () => {
      let config = firebaseConfig()
      if ( config.storageBucket ) {
        return config.storageBucket.split( '.' )[ 0 ]
      } else if ( config.authDomain ) {
        return config.authDomain.split( '.' )[ 0 ]
      } else if ( config.databaseURL ) {
        return config.databaseURL.split( '://' )[ 1 ].split( '.' )[ 0 ]
      } else {
        return null
      }
    }
    _project.id = projectId

    /*
     * Project Context
     * #project-context
     */

    _project.context = {}

    /** @function project.context.initialize
     * Initializes the `context` object for a given project. This mainly involves initializing all the dependencies that require a project id and credentials.
     * @param {Object} params - The parameter object.
     * @param {string} params.projectId - The id for the project you wish to act on behalf of.
     * @param {Object} params.serviceAccount - (Optional) JSON formatted service account data. Will default to a service account passed in when loading the main library.
     * @returns {Object} A context object used when initializing libraries. Contains project specific intialized versions of `firebase-admin`, `@google-cloud/pubsub` and `@google-cloud/storage`. Also has newly instantiated `libs`.
     */
    let initializeProjectContext = ( params = {} ) => {
      let projectContext = params.context || context
      let projectId = params.projectId
      let serviceAccount = params.serviceAccount || context.serviceAccount

      projectContext.admin = initializeAdmin( {
        serviceAccount: serviceAccount,
        projectId: projectId
      } )

      projectContext.firebase = initializeFirebase( {
        apiKey: params.apiKey,
        projectId: projectId
      } )

      projectContext.pubsub = initializePubsub( {
        serviceAccount: serviceAccount,
        projectId: projectId
      } )

      projectContext.storage = initializeStorage( {
        serviceAccount: serviceAccount,
        projectId: projectId
      } )

      projectContext.libs = require( './index' )( projectContext )( settings )

      return projectContext
    }
    _project.context.initialize = initializeProjectContext

    /*
     * Project Database
     * #project-database
     */

    _project.database = {}

    /** @function project.database.url
     * Gets the Firebase Realtime Database URL for a project.
     * @param {Object} params - The parameter object.
     * @param {string} params.projectId - The id for the project you wish to act on behalf of. Defaults to local project.
     * @returns {string} The firebase database url for the initialized/current project.
     */
    let projectDatabaseURL = ( params = {} ) => {
      return databaseURL( { projectId: params.projectId || projectId() } )
    }
    _project.database.url = projectDatabaseURL

    /*
     * Project Functions
     * #project-functions
     */

    _project.functions = {}

    /** @function project.functions.url
     * Gets the Firebase Functions URL for a project.
     * @param {Object} params - The parameter object.
     * @param {string} params.projectId - The id for the project you wish to act on behalf of. Defaults to local project.
     * @returns {string} The firebase database url for the initialized/current project.
     */
    let projectFunctionsURL = ( params = {} ) => {
      return functionsURL( { projectId: params.projectId || projectId() } )
    }
    _project.functions.url = projectFunctionsURL

    /*
     * Pub/Sub
     * #pubsub
     */

    let _pubsub = {}

    /** @function pubsub.initialize
       * Initializes the `@google-cloud/pubsub` library for a given project.
       * @param {Object} params - The parameter object.
       * @param {string} params.projectId - The id for the project you wish to act on behalf of.
       * @param {Object} params.serviceAccount - (Optional) JSON formatted service account data. Will default to a service account passed in when loading the main library.
       * @returns {Object} An initialized `@google-cloud/pubsub` library.
       */
    let initializePubsub = ( params = {} ) => {
      return pubsubAPI( {
        credentials: params.serviceAccount || context.serviceAccount,
        projectId: params.projectId
      } )
    }
    _pubsub.initialize = initializePubsub

    /** @function publish
     * Publish a message to a Pub/Sub topic.
     * @param {Object} params - The parameter object.
     * @param {Object} params.attributes - The attributes for the message.
     * @param {String} params.attributes._id - The internal id used to track the message between functions. This is generated if not provided.
     * @param {Object} params.message - The message to be published.
     * @param {string} params.topic - The topic to be published to.
     * @returns {Promise} Contains string of published message id or error.
     */
    let publishPubsub = ( params = {} ) => {
      let _methodName = 'pubsub-publish'
      // Sign attributes with own project id
      let attributes = params.attributes || {}

      attributes._requester = projectId()
      attributes._id = attributes._id || uuid()

      let message = Buffer.from( stringify( params.message ) )

      if ( settings.logging !== false ) { console.log( `Publishing to ${params.topic} with internal id: ${attributes._id}` ) }

      return context.pubsub
        .topic( params.topic )
        .publisher()
        .publish( message, attributes )
        .catch( error => {
          return createError( {
            error: error,
            message: 'Could not publish to topic.',
            name: _methodName,
            info: {
              https: { code: 400 },
              package: '@google-cloud/pubsub',
              function: {
                topic: params.topic,
                publisher: null,
                publish: {
                  message: message,
                  attributes: attributes
                }
              }
            }
          } )
        } )
    }
    _pubsub.publish = publishPubsub

    /*
     * Storage
     * #storage
     */

    let _storage = {}

    /** @function storage.initialize
     * Initializes the `@google-cloud/storage` library for a given project.
     * @param {Object} params - The parameter object.
     * @param {string} params.projectId - The id for the project you wish to act on behalf of.
     * @param {Object} params.serviceAccount - (Optional) JSON formatted service account data. Will default to a service account passed in when loading the main library.
   * @returns {Object} An initialized `@google-cloud/storage` library.
     */
    let initializeStorage = ( params = {} ) => {
      return storageAPI( {
        credentials: params.serviceAccount || context.serviceAccount,
        projectId: params.projectId
      } )
    }
    _storage.initialize = initializeStorage

    let storageBucket = ( params = {} ) => { return `${params.projectId || projectId()}.appspot.com` }
    _storage.bucket = storageBucket

    /*
     * User
     * #user
     */

    let _user = {}

    let fetchUser = uid => { return databaseFetch( { path: userPath( uid ) } ) }
    _user.fetch = fetchUser

    /*
     * User Claims
     * #user-claims
     */

    _user.claims = {}

    // #refactor - legacy function export, migrated to `auth`
    _user.claims.set = setAuthClaims

    let verifyUserClaims = ( params = {} ) => {
      let key = params.key
      let value = params.value || true
      let userData = params.userData || {}
      let claims = userData.claims || {}

      if ( claims[ key ] === value ) {
        return true
      } else {
        return new VError( { name: 'verify-claims' }, 'User does not have sufficient claims.' )
      }
    }
    _user.claims.verify = verifyUserClaims

    /*
     * User Anonymous
     * #user-anonymous
     */

    let userIsAnonymous = ( params = {} ) => {
      let provider = params.decodedIdToken.firebase.sign_in_provider
      if ( provider === 'anonymous' ) { return true }
      return false
    }
    _user.isAnonymous = userIsAnonymous

    /*
     * User Database
     * #user-database
     */

    _user.database = {}

    let userPath = id => { return `user/${id}` }
    _user.database.path = userPath

    /*
     * User Database Address
     * #user-database-address
     */

    _user.database.address = {}

    let userAddressPath = ( params = {} ) => {
      let addressId = params.addressId
      let path = userPath( params.uid ) + '/address'
      if ( addressId ) { path += `/${addressId}` }
      return path
    }
    _user.database.address.path = userAddressPath

    return {
      admin: _admin,
      auth: _auth,
      cart: _cart,
      database: _database,
      error: _error,
      firebase: _firebase,
      functions: _functions,
      invoice: _invoice,
      jwt: _jwt,
      localization: _localization,
      log: _log,
      product: _product,
      project: _project,
      pubsub: _pubsub,
      storage: _storage,
      user: _user
    }
  }
}
