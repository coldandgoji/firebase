const pubsubAPI = require( '@google-cloud/pubsub' )
const storageAPI = require( '@google-cloud/storage' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    console.log( 'Initializing "@coldandgoji/firebase"...' )

    /*
     * Environment Variables
     */
    console.log( 'Loading environment...' )
    context.env = context.functions.config()

    console.log( 'Environment:', context.env )
    console.log( 'Settings:', settings )

    /*
     * Setup
     * Temporary instance of local libraries.
     */

    let setupLibs = require( './libs/index' )( context )( settings )

    /*
     * Firebase
     * Include initialized Firebase packages
     */
    console.log( 'Loading "firebase"...' )
    context.firebase = setupLibs.firebase.initialize()

    console.log( 'Loading "firebase-admin"...' )
    context.admin = setupLibs.admin.initialize()

    /*
     * Google Cloud APIs
     * All Google Cloud API packages utilized in this package.
     */
    console.log( 'Loading "@google-cloud/pubsub"...' )
    context.pubsub = pubsubAPI()

    console.log( 'Loading "@google-cloud/storage"...' )
    context.storage = storageAPI()

    /*
     * Local Libraries
     * Common libraries for the package.
     *
     * Requires `context` with:
     *   - env
     *   - pubsub
     *   - storage
     *   - firebase
     *   - admin
     *
     * Now that `firebase-admin` has been initialized and included in the context object,
     * the libs may be added.
     *
     * Some library functions require an initialized `firebase-admin`, along with packages previously added
     * to the context object.
     */
    console.log( 'Loading local libraries...' )
    context.libs = require( './libs/index' )( context )( settings )

    /*
     * Firebase Trigger Libraries
     * Libraries to be used when creating Firebase Functions for a given trigger.
     *
     * Requires `context` with:
     *   - env
     *   - pubsub
     *   - storage
     *   - firebase
     *   - admin
     *   - libs
     */
    console.log( 'Loading triggers...' )
    context.triggers = require( './source/index' )( context )( settings )

    /*
     * Module Exports
     */

    console.log( 'Initialization complete for "@coldandgoji/firebase".' )
    return context
  }
}
